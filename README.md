Zaawansowane techniki projektowania gier 2017/2018

3. Design i multiplayer (lokalny/P2P)
    b) System dialogów z możliwością wyboru kwestii przez każdego z graczy oraz wpływem wyborów na fabułę
        - Minimum: Różne zakończenia w zależności od łańcucha wyborów (co najmniej 3)
        - Dodatkowo: P2P i większy nacisk na estetykę/fabułę
        
Unity wersja 5.4.03f Win x64 https://unity3d.com/get-unity/download?thank-you=update&download_nid=41345&os=Win

wersja Linux DEB 
Official Installers for 64-bit Ubuntu Linux:
5.4.0f3: http://download.unity3d.com/download_unity/linux/unity-editor-5.4.0f3+20160727_amd64.deb
(sha1sum 8f0540a95445cb50c592493d0b54f17f05389b85)