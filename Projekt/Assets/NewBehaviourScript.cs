﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour {
    [SerializeField]
    private int miejsceWdialogu=0;
    private bool init = true;
    private bool init2 = true;

    public Text Pyt;
    public Text Odp;
    public Text Poz3;
    public Text Poz4;
    public Text Opis5;
    // Use this for initialization
    void Start () {
        Debug.Log("Start");
    }

    void initDialogs()
    {
        if (init)
        {
            Debug.Log("initDialogs");

            Pyt.text = "";
            Odp.text = "";
            Poz3.text = "";
            Poz4.text = "";
            Opis5.text = "";

            wyswietlNapis(@"Jarosław wraca do domu przed północą z objawami choroby filipińskiej. Liczył na to, że jego występek umknie czujnemu oku Krystyny – od 25 lat kobiety jego życia. Zamiast tego…
               Press space to start", 5);
            init = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            wyswietlNapis("", 5);
            proceedWithDialog(-1);
            init2 = false;
        }
    }

    void wyswietlNapis(string napis,int poz)
    {
        
        //Debug.Log(napis); //to bedzie można potem zmodyfikowac sposob wyswietlania napisow
        switch(poz)
        {
            case 1:
            Pyt.text = napis;
                break;
            case 2:
                Odp.text = napis;
                break;
            case 3:
              
                Poz3.text = napis;
                break;
            case 4:
                Poz4.text = napis;
                break;
            case 5:
                Opis5.text = napis;
                break;

        }
        
    }

    // Update is called once per frame
    void Update () {

        if(init2) {
            initDialogs();
        }

        // Debug.Log("NewBehaviourScript update");
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            //Debug.Log("1");
            proceedWithDialog(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            //Debug.Log("2");
            proceedWithDialog(2);
            miejsceWdialogu = 2;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
           // Debug.Log("3");
            proceedWithDialog(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
           // Debug.Log("4");
            proceedWithDialog(4);
        }
    }

    void proceedWithDialog(int odp)
    {
       // Debug.Log("proceedWithDialog");
        //Debug.Log(odp);
        //Debug.Log(miejsceWdialogu);

        //tutaj będzie milion if'ów
        //w zależności od wybranej odpowiedzi (przekazana jako argument) i miejsca w dialogu - zmienna
        //miejsceWdialogu przejdz w dialogu dalej i wyświetl jakieś napisy (na razie do konsoli)
        if (miejsceWdialogu==0)
        {
            wyswietlNapis("Skończyłeś zmianę w południe, gdzieś ty się podziewał stary niewdzięczniku?! Jak ty wyglądasz?! Znowu wracasz pijany! Dobrze, że Sebuś i Dżesika już śpią…", 1);
            wyswietlNapis(@"1. Królowo złota, kierownik kazał nadgodziny wziąć. Jak mogłem odmówić… potem przyszła Halinka z urodzinowym toastem. Ale przysięgam, że tylko jednego, nic więcej!
2. Kryśka, daj mi spokój! Z chłopakami z taśmy poszliśmy na mecz. Wielkie mi mecyje… Od świtu do nocy człowiek wali młotem, kręci śruby, chłodno, głodno, co miesiąc wszystko do domu przynosi i taka jeszcze wyrzuty robi. Co mnie podkusiło, że się z tobą ożeniłem… Postaw lepiej kefirek przy łóżku i już daj mi spokój!
3. Ja pijany?! Ty lepiej sama stań przed lustrem. Jak ty w ogóle wyglądasz? Mówiłem, żebyś oddała dzieci sąsiadce i do nas przyszła. Kiedyś razem balowaliśmy, a teraz nic tylko w domu siedzisz. Dobrze, że Halina miała dzisiaj urodziny i mogłem się trochę rozerwać!", 2);
            miejsceWdialogu = 1;
        }
       else if (miejsceWdialogu == 1 && odp == 1)
        { wyswietlNapis("Idźże spać ochleju, jeden tylko a się słaniasz, ten twój jeden to jak u innego z litr. Idź do łożka znaim dzieci pobudzisz. Jeszcze ta Halinka nieszczęsna, idźże no bo ogniem zioniesz!",1);
          wyswietlNapis(@"1. Krystynko kochana, juśśśśś idę. Wiesz, że tak tylko kapeczkę, Halinka to ta parszywa wiedźma, nawet ta wódka co przyniosła, to się pić nie dało. Idę już idę spać, tylko nie krzycz już!
2. Kryśka, łeb mnie już zaczyna boleć od tego krzyku twojego, od ochlei jeszcze wyzywasz… taka niewdzięczna jesteś, a ja tyle dobrego robie...
3. Kryśka weź już nie ględź tak, ogniem niby zione a Ty jadem!", 2);
            miejsceWdialogu = 2;
            
        }
        else if (miejsceWdialogu == 1 && odp == 2)
        {
            wyswietlNapis("Spokój??? Ja Ci zaraz pokaże czym jest spokój! Dosyć mam twojego pijaństwa! A kefirem to w łeb dostaniesz zaraz!",1);
            wyswietlNapis(@"1. Krystyna! Ty na mnie nie wrzeszcz! Ja kulturanie się napiłem, a tu pretensje! Haruje ciągle, należy mi się!
2. Nic tylko się na mnie wydzierasz! (płacze) ja tak się staram, dzieci zawsze wszystko mają, Tobie nic nie brakuje a Ty mnie tak gnębisz…kefirem w łeb rzucać… Halinka to nawet wódeczkę przyniosła a Ty kefirku dać nie chcesz...
3. Kefirem w łeb?? Za moje pieniądze ten kefir jest! Nie myśl sobie, że taka zaradna jesteś! Gdyby nie ja, nic by w tym domu nie było!", 2);
            miejsceWdialogu = 2;
            
        }
         else if (miejsceWdialogu == 1 && odp == 3)
        {
            wyswietlNapis("Dzieci sąsiadce? Jak ja wyglądam?? Pijaku jeden! Na siebie spójrz! No nie, tym razem Ci nie odpuszcze! Ja tu cały dzień przy garach stoje, dzieci wychowuje a Ty nic, tylko wstyd w mieście przynosisz! Pijany jak bela chodzisz, baby jakieś obmacujesz obce, moczymordo wstrętna Ty!",1);
            wyswietlNapis(@"1. No trochu się człowiek napił a zaraz, że pijak. W życiu bym nic nie obmacał, no Krystynko chyba, że Ciebie! To zawsze!
2. Krystyyyyyna Ty mnie lepiej nie prowokuj, zapraszałem cię przecież a Ty nic tylko gary i dzieci, no już dobrze wyglądasz, ale te wałki mogłabyś zdjąć,  no już, nie krzycz tak, bo Sebusia obudzisz, znowu będzie płakał, że matka nic tylko krzyczy...
3. Od pijaków mnie wyzywasz?! Taka mocna jesteś?! A ty co? Nic nie robisz tylko siedzisz na dupie całymi dniami i plotkujesz tylko z tymi koleżaneczkami. TO JA TU HARUJE JAK WÓŁ, NALEŻY MI SIĘ, Ty nic nie robisz! Brudno, dzieci same jedynki w szkole. Halinka to przynajmniej jak kobieta wygląda!",2);
            miejsceWdialogu = 2;
            
        }
         else if (miejsceWdialogu == 1 && odp == 1)
        {
            wyswietlNapis("Obiecałeś się zmienić. Mówiłeś, że zaopiekujesz się nami i domem... Nie mam już do tego siły, wchodź do środka. Masz ostatnią szansę!",1);
            wyswietlNapis(@"1. Krystyna przestan się wygłupiać. Jak było ostatnio kiedy zostałem sam z dzieciakami, miałaś wyjść na chwilę do sąsiadki, a wróciłaś po 2 dniach. Przyganiał kocioł garnkowi...
            2. Halin... pfff. Krysiu, ostatni raz, nigdy więcej się to nie powtórzy... Wszystko będzie tak, jak sobie zażyczysz.",2);
            miejsceWdialogu = 2;
            
        }
        else if (miejsceWdialogu == 1 && (odp == 2 || odp == 3))
        {
            wyswietlNapis("Miałeś się zmienić, a wyszło jak zawsze. Psu na budę twoje obietnice! Jesteś moją życiową porażką!",1);
            wyswietlNapis(@"1.Krysiu, no wiesz. Myślałem, że jesteś ze mną szczęśliwa. No nie gniewaj się już, przecież wiesz, że ja dla ciebie zrobię wszystko.
            2. Jak możesz, Halinka nigdy by mi tak nie powiedziała! Przestań się zgłościć i chodźmy spać, stęskniłem się za tobą...",2);
            miejsceWdialogu = 2;
            
        }
        else if (miejsceWdialogu == 2 && odp == 1)
        {
            wyswietlNapis("Przestań, przestań, za dużo cukru. Rano to ja z tobą porozmawiam...",1);
            wyswietlNapis("", 2);
            miejsceWdialogu = 2;
            
        }
        else if (miejsceWdialogu == 2 && odp == 2)
        {
            wyswietlNapis("Halinki ci się zachciało?! Droga wolna, nikt cię tu nie trzyma. Tu masz walizkę, a tam są drzwi!",1);
            wyswietlNapis("", 2);
            miejsceWdialogu = 2;
            
        }  
          }

    
}
