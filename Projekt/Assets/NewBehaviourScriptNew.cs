﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewBehaviourScriptNew : MonoBehaviour {

    public Sprite img1;
    public Sprite img2;
    public Sprite img3;

    // Use this for initialization
    void Start () {
        Debug.Log("Start");
    }

    // Update is called once per frame
    void Update () {

        // Debug.Log("NewBehaviourScript update");
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("SpriteRenderer 1");
            this.GetComponent<SpriteRenderer>().sprite = img1; 
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Debug.Log("2");
            this.GetComponent<SpriteRenderer>().sprite = img2;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Debug.Log("3");
            this.GetComponent<SpriteRenderer>().sprite = img3;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Debug.Log("4");
           // this.GetComponent<SpriteRenderer>().sprite = null;
        }
    }
}
