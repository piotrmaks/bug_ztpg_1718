﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour {

    public Animator _anim;
    public Text text;
    private ExampleStateMachineBehaviour exampleSmb;
    private NowySwiat ns;

    // Use this for initialization
    void Start () {
        Debug.Log("TextController Enter");
        text.text = "Hello world";
        exampleSmb = _anim.GetBehaviour<ExampleStateMachineBehaviour>();
        exampleSmb.exampleMb = this;

        ns = _anim.GetBehaviour<NowySwiat>();
        ns.exampleMb = this;
    }
	
	// Update is called once per frame
	void Update () {
	 /*   if (Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "Space key pressed";
        } */
	}
}
