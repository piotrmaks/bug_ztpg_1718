﻿using UnityEngine;
using System.Collections;

public class ExampleStateMachineBehaviour : StateMachineBehaviour {

    public TextController exampleMb;
    public TextController odpowiedzi;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        exampleMb.text.text = "Pytanie";
        odpowiedzi.text.text = 
            @"Odp1
              Odp2
              Odp3
              Odp4";

        animator.SetBool("odp_1", false);
        animator.SetBool("odp_2", false);
        animator.SetBool("odp_3", false);
        animator.SetBool("odp_4", false);
    }
}
